<nav class="navbar navbar-expand-lg">
    <div class="container-xl">
        <!-- site logo -->
        <a class="navbar-brand" href="index.html"><img
                src="<?php echo get_template_directory_uri() ?>/assets/images/logo.svg" alt="logo"/></a>

        <div class="collapse navbar-collapse">
            <!-- menus -->
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown active">
                    <a class="nav-link dropdown-toggle" href="index.html">صفحه اصلی</a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="index.html">صفحه اصلی 1</a></li>
                        <li><a class="dropdown-item" href="index-2.html">صفحه اصلی 2</a></li>
                        <li><a class="dropdown-item" href="index-3.html">صفحه اصلی 3</a></li>
                        <li><a class="dropdown-item" href="index-4.html">صفحه اصلی 4</a></li>
                        <li><a class="dropdown-item" href="index-5.html">صفحه اصلی 5</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#">صفحات</a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="category.html">دسته بندی</a></li>
                        <li><a class="dropdown-item" href="blog-single.html">جزییات وبلاگ 1</a></li>
                        <li><a class="dropdown-item" href="blog-single-2.html">جزییات وبلاگ 2</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="contact.html">تماس با ما</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="about.html">درباره ما</a>
                </li>
            </ul>
        </div>

        <!-- header right section -->
        <div class="header-right">
            <!-- social icons -->
            <ul class="social-icons list-unstyled list-inline mb-0">
                <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                <li class="list-inline-item"><a href="#"><i class="fab fa-twitter"></i></a></li>
                <li class="list-inline-item"><a href="#"><i class="fab fa-instagram"></i></a></li>
                <li class="list-inline-item"><a href="#"><i class="fab fa-pinterest"></i></a></li>
                <li class="list-inline-item"><a href="#"><i class="fab fa-medium"></i></a></li>
                <li class="list-inline-item"><a href="#"><i class="fab fa-youtube"></i></a></li>
            </ul>
            <!-- header buttons -->
            <div class="header-buttons">
                <button class="search icon-button">
                    <i class="icon-magnifier"></i>
                </button>
                <button class="burger-menu icon-button">
                    <span class="burger-icon"></span>
                </button>
            </div>
        </div>
    </div>
</nav>