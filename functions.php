<?php
/**
 * Theme Function
 *
 * @package Katen WordPress
 */

if (!defined('KATEN_DIR_PATH'))
{
    define('KATEN_DIR_PATH',untrailingslashit(get_template_directory()));
}
print_r(KATEN_DIR_PATH);
wp_die();
require_once KATEN_DIR_PATH . '/inc/helpers/autoloader.php';
function katen_enqueue_style()
{
    wp_enqueue_style('bootstrap.min', get_stylesheet_directory_uri(). '/assets/css/bootstrap.min.css',false, 'all');
    wp_enqueue_style('all', get_stylesheet_directory_uri() . '/assets/css/all.min.css',false, 'all');
    wp_enqueue_style('slick', get_stylesheet_directory_uri() . '/assets/css/slick.css',false, 'all');
    wp_enqueue_style('simple-line-icons', get_stylesheet_directory_uri() . '/assets/css/simple-line-icons.css',false, 'all');
    wp_enqueue_style('vazir-font', get_stylesheet_directory_uri(). '/assets/css/vazir-font.css',false, 'all');
    wp_enqueue_style('style', get_stylesheet_uri(), [], filemtime(get_template_directory() . '/style.css'), 'all');
}

function katen_enqueue_script()
{
    wp_enqueue_script('jquery.min', get_template_directory_uri() . '/assets/js/jquery.min.js', [],false, false);
    wp_enqueue_script('popper', get_template_directory_uri() . '/assets/js/popper.min.js', [], false, true);
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', [],false, true);
    wp_enqueue_script('slick', get_template_directory_uri() . '/assets/js/slick.min.js', [], false, true);
    wp_enqueue_script('jquery.sticky-sidebar', get_template_directory_uri() . '/assets/js/jquery.sticky-sidebar.min.js', [], false, true);
    wp_enqueue_script('custom', get_template_directory_uri() . '/assets/js/custom.js', ['jquery.min'], filemtime(get_template_directory() . '/assets/js/custom.js'), true);
}

add_action('wp_enqueue_scripts', 'katen_enqueue_style');
add_action('wp_enqueue_scripts', 'katen_enqueue_script');
?>