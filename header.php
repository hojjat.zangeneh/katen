<?php
/**
 * Header file.
 *
 * @package Katen WordPress
 */
?>
<?php
/**
 *  Main template file
 * @package Katen
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php get_template_part('template-part/header/head') ?>
<body <?php body_class(); ?>>
<?php if (function_exists('wp_body_open')) {
    wp_body_open();
} ?>
<?php get_template_part('template-part/header/preloader') ?>

<!-- site wrapper -->
<div class="site-wrapper">
    <div class="main-overlay"></div>
    <!-- header -->
    <header class="header-default">
        <?php get_template_part('template-part/header/nav') ?>
    </header>

