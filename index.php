<?php get_header() ?>
<!-- hero section -->
<section id="hero">

    <div class="container-xl">

        <div class="row gy-4">

            <div class="col-lg-8">

                <!-- featured post large -->
                <div class="post featured-post-lg">
                    <div class="details clearfix">
                        <a href="category.html" class="category-badge">الهام بخش</a>
                        <h2 class="post-title"><a href="blog-single.html">5 راه آسان که می توانید آینده را به موفقیت
                                تبدیل کنید</a></h2>
                        <ul class="meta list-inline mb-0">
                            <li class="list-inline-item"><a href="#">کاتن</a></li>
                            <li class="list-inline-item">30 اردیبهشت 1401</li>
                        </ul>
                    </div>
                    <a href="blog-single.html">
                        <div class="thumb rounded">
                            <div class="inner data-bg-image"
                                 data-bg-image="<?php echo get_template_directory_uri() ?>/assets/images/posts/featured-lg.jpg"></div>
                        </div>
                    </a>
                </div>

            </div>
            <div class="col-lg-4">

                <!-- post tabs -->
                <div class="post-tabs rounded bordered">
                    <!-- tab navs -->
                    <ul class="nav nav-tabs nav-pills nav-fill" id="مطالبTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button aria-controls="محبوب" aria-selected="true" class="nav-link active"
                                    data-bs-target="#محبوب" data-bs-toggle="tab" id="محبوب-tab" role="tab"
                                    type="button">محبوب
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button aria-controls="اخیر" aria-selected="false" class="nav-link"
                                    data-bs-target="#اخیر" data-bs-toggle="tab" id="اخیر-tab" role="tab"
                                    type="button">اخیر
                            </button>
                        </li>
                    </ul>
                    <!-- tab contents -->
                    <div class="tab-content" id="مطالبTabContent">
                        <!-- loader -->
                        <div class="lds-dual-ring"></div>
                        <!-- مطالب محبوب -->
                        <div aria-labelledby="محبوب-tab" class="tab-pane fade show active" id="محبوب"
                             role="tabpanel">
                            <!-- post -->
                            <div class="post post-list-sm circle">
                                <div class="thumb circle">
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/tabs-1.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="blog-single.html">3 راه آسان برای سریعتر
                                            کردن آیفون</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- post -->
                            <div class="post post-list-sm circle">
                                <div class="thumb circle">
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/tabs-2.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="blog-single.html">یک روش فوق العاده آسان که
                                            برای همه کار می کند</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- post -->
                            <div class="post post-list-sm circle">
                                <div class="thumb circle">
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/tabs-3.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="blog-single.html">10 راه برای شروع فوری
                                            فروش مبلمان</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- post -->
                            <div class="post post-list-sm circle">
                                <div class="thumb circle">
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/tabs-4.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="blog-single.html">15 راه ناشنیده برای
                                            دستیابی به واکر بزرگ</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- اخیر مطالب -->
                        <div aria-labelledby="اخیر-tab" class="tab-pane fade" id="اخیر" role="tabpanel">
                            <!-- post -->
                            <div class="post post-list-sm circle">
                                <div class="thumb circle">
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/tabs-2.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="blog-single.html">یک روش فوق العاده آسان که
                                            برای همه کار می کند</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- post -->
                            <div class="post post-list-sm circle">
                                <div class="thumb circle">
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/tabs-1.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="blog-single.html">3 راه آسان برای سریعتر
                                            کردن آیفون</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- post -->
                            <div class="post post-list-sm circle">
                                <div class="thumb circle">
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/tabs-4.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="blog-single.html">15 راه ناشنیده برای
                                            دستیابی به واکر بزرگ</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- post -->
                            <div class="post post-list-sm circle">
                                <div class="thumb circle">
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/tabs-3.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="blog-single.html">10 راه برای شروع فوری
                                            فروش مبلمان</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</section>

<!-- section main content -->
<section class="main-content">
    <div class="container-xl">

        <div class="row gy-4">


            <div class="col-lg-8">

                <!-- section header -->
                <div class="section-header">
                    <h3 class="section-title">پیشنهاد سردبیر</h3>
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/wave.svg" class="wave"
                         alt="wave"/>
                </div>

                <div class="padding-30 rounded bordered">
                    <div class="row gy-5">
                        <div class="col-sm-6">
                            <!-- post -->
                            <div class="post">
                                <div class="thumb rounded">
                                    <a href="category.html" class="category-badge position-absolute">سبک زندگی</a>
                                    <span class="post-format">
											<i class="icon-picture"></i>
										</span>
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/editor-lg.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <ul class="meta list-inline mt-4 mb-0">
                                    <li class="list-inline-item"><a href="#"><img
                                                    src="<?php echo get_template_directory_uri() ?>/assets/images/other/author-sm.png"
                                                    class="author" alt="author"/>کاتن</a>
                                    </li>
                                    <li class="list-inline-item">30 اردیبهشت 1401</li>
                                </ul>
                                <h5 class="post-title mb-3 mt-3"><a href="blog-single.html">15 راه ناشنیده برای
                                        دستیابی به واکر بزرگ</a></h5>
                                <p class="excerpt mb-0">آرامشی شگفت انگیز همه جانم را فرا گرفته است، مثل این صبح های
                                    شیرین بهاری که از آن لذت می برم</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <!-- post -->
                            <div class="post post-list-sm square">
                                <div class="thumb rounded">
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/editor-sm-1.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="blog-single.html">3 راه آسان برای سریعتر
                                            کردن آیفون</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- post -->
                            <div class="post post-list-sm square">
                                <div class="thumb rounded">
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/editor-sm-2.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="blog-single.html">یک روش فوق العاده آسان که
                                            برای همه کار می کند</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- post -->
                            <div class="post post-list-sm square">
                                <div class="thumb rounded">
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/editor-sm-3.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="blog-single.html">10 راه برای شروع فوری
                                            فروش مبلمان</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- post -->
                            <div class="post post-list-sm square">
                                <div class="thumb rounded">
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/editor-sm-4.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="blog-single.html">15 راه ناشنیده برای
                                            دستیابی به واکر بزرگ</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="spacer" data-height="50"></div>

                <!-- horizontal ads -->
                <div class="ads-horizontal text-md-center">
                    <span class="ads-title">- تبلیغات -</span>
                    <a href="#">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/ads/ad-750.png"
                             alt="Advertisement"/>
                    </a>
                </div>

                <div class="spacer" data-height="50"></div>

                <!-- section header -->
                <div class="section-header">
                    <h3 class="section-title">پرطرفدار</h3>
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/wave.svg" class="wave"
                         alt="wave"/>
                </div>

                <div class="padding-30 rounded bordered">
                    <div class="row gy-5">
                        <div class="col-sm-6">
                            <!-- post large -->
                            <div class="post">
                                <div class="thumb rounded">
                                    <a href="category.html" class="category-badge position-absolute">فرهنگ</a>
                                    <span class="post-format">
											<i class="icon-picture"></i>
										</span>
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/trending-lg-1.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <ul class="meta list-inline mt-4 mb-0">
                                    <li class="list-inline-item"><a href="#"><img
                                                    src="<?php echo get_template_directory_uri() ?>/assets/images/other/author-sm.png"
                                                    class="author" alt="author"/>کاتن</a>
                                    </li>
                                    <li class="list-inline-item">30 اردیبهشت 1401</li>
                                </ul>
                                <h5 class="post-title mb-3 mt-3"><a href="blog-single.html">حقایقی در مورد تجارت که
                                        به موفقیت شما کمک می کند</a></h5>
                                <p class="excerpt mb-0">آرامشی شگفت انگیز همه جانم را فرا گرفته است، مثل این صبح های
                                    شیرین بهاری که از آن لذت می برم</p>
                            </div>
                            <!-- post -->
                            <div class="post post-list-sm square before-seperator">
                                <div class="thumb rounded">
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/trending-sm-1.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="blog-single.html">3 راه آسان برای سریعتر
                                            کردن آیفون</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- post -->
                            <div class="post post-list-sm square before-seperator">
                                <div class="thumb rounded">
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/trending-sm-2.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="blog-single.html">یک روش فوق العاده آسان که
                                            برای همه کار می کند</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <!-- post large -->
                            <div class="post">
                                <div class="thumb rounded">
                                    <a href="category.html" class="category-badge position-absolute">الهام بخش</a>
                                    <span class="post-format">
											<i class="icon-earphones"></i>
										</span>
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/trending-lg-2.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <ul class="meta list-inline mt-4 mb-0">
                                    <li class="list-inline-item"><a href="#"><img
                                                    src="<?php echo get_template_directory_uri() ?>/assets/images/other/author-sm.png"
                                                    class="author" alt="author"/>کاتن</a>
                                    </li>
                                    <li class="list-inline-item">30 اردیبهشت 1401</li>
                                </ul>
                                <h5 class="post-title mb-3 mt-3"><a href="blog-single.html">5 راه آسان که می توانید
                                        آینده را به موفقیت تبدیل کنید</a></h5>
                                <p class="excerpt mb-0">آرامشی شگفت انگیز همه جانم را فرا گرفته است، مثل این صبح های
                                    شیرین بهاری که از آن لذت می برم</p>
                            </div>
                            <!-- post -->
                            <div class="post post-list-sm square before-seperator">
                                <div class="thumb rounded">
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/trending-sm-3.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="blog-single.html">در اینجا 8 راه برای
                                            موفقیت سریعتر وجود دارد</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- post -->
                            <div class="post post-list-sm square before-seperator">
                                <div class="thumb rounded">
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/trending-sm-4.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="blog-single.html">با این 7 نکته بر هنر
                                            طبیعت مسلط شوید</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="spacer" data-height="50"></div>

                <!-- section header -->
                <div class="section-header">
                    <h3 class="section-title">الهام بخش</h3>
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/wave.svg" class="wave"
                         alt="wave"/>
                    <div class="slick-arrows-top">
                        <button type="button" data-role="none" class="carousel-topNav-prev slick-custom-buttons"
                                aria-label="بعدی"><i class="icon-arrow-right"></i></button>
                        <button type="button" data-role="none" class="carousel-topNav-next slick-custom-buttons"
                                aria-label="قبلی"><i class="icon-arrow-left"></i></button>
                    </div>
                </div>

                <div class="row post-carousel-twoCol post-carousel">
                    <!-- post -->
                    <div class="post post-over-content col-md-6">
                        <div class="details clearfix">
                            <a href="category.html" class="category-badge">الهام بخش</a>
                            <h4 class="post-title"><a href="blog-single.html">آیا می خواهید خالکوبی جذاب تری داشته
                                    باشید؟</a></h4>
                            <ul class="meta list-inline mb-0">
                                <li class="list-inline-item"><a href="#">کاتن</a></li>
                                <li class="list-inline-item">30 اردیبهشت 1401</li>
                            </ul>
                        </div>
                        <a href="blog-single.html">
                            <div class="thumb rounded">
                                <div class="inner">
                                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/inspiration-1.jpg"
                                         alt="thumb"/>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- post -->
                    <div class="post post-over-content col-md-6">
                        <div class="details clearfix">
                            <a href="category.html" class="category-badge">الهام بخش</a>
                            <h4 class="post-title"><a href="blog-single.html">با کمک این ۷ نکته احساس بهتری داشته
                                    باشید:</a></h4>
                            <ul class="meta list-inline mb-0">
                                <li class="list-inline-item"><a href="#">کاتن</a></li>
                                <li class="list-inline-item">30 اردیبهشت 1401</li>
                            </ul>
                        </div>
                        <a href="blog-single.html">
                            <div class="thumb rounded">
                                <div class="inner">
                                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/inspiration-2.jpg"
                                         alt="thumb"/>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- post -->
                    <div class="post post-over-content col-md-6">
                        <div class="details clearfix">
                            <a href="category.html" class="category-badge">الهام بخش</a>
                            <h4 class="post-title"><a href="blog-single.html">نور شما نزدیک به توقف شدن است:</a>
                            </h4>
                            <ul class="meta list-inline mb-0">
                                <li class="list-inline-item"><a href="#">کاتن</a></li>
                                <li class="list-inline-item">30 اردیبهشت 1401</li>
                            </ul>
                        </div>
                        <a href="blog-single.html">
                            <div class="thumb rounded">
                                <div class="inner">
                                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/inspiration-3.jpg"
                                         alt="thumb"/>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="spacer" data-height="50"></div>

                <!-- section header -->
                <div class="section-header">
                    <h3 class="section-title">آخرین مطالب</h3>
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/wave.svg" class="wave"
                         alt="wave"/>
                </div>

                <div class="padding-30 rounded bordered">

                    <div class="row">

                        <div class="col-md-12 col-sm-6">
                            <!-- post -->
                            <div class="post post-list clearfix">
                                <div class="thumb rounded">
										<span class="post-format-sm">
											<i class="icon-picture"></i>
										</span>
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/latest-sm-1.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details">
                                    <ul class="meta list-inline mb-3">
                                        <li class="list-inline-item"><a href="#"><img
                                                        src="<?php echo get_template_directory_uri() ?>/assets/images/other/author-sm.png"
                                                        class="author"
                                                        alt="author"/>کاتن</a></li>
                                        <li class="list-inline-item"><a href="#">پرطرفدار</a></li>
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                    <h5 class="post-title"><a href="blog-single.html">60 کاری که باید فوراً در مورد
                                            ساختمان انجام دهید</a></h5>
                                    <p class="excerpt mb-0">آرامشی شگفت انگیز تمام روحم را گرفته است، مثل این صبح
                                        های شیرین</p>
                                    <div class="post-bottom clearfix d-flex align-items-center">
                                        <div class="social-share me-auto">
                                            <button class="toggle-button icon-share"></button>
                                            <ul class="icons list-unstyled list-inline mb-0">
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="fab fa-facebook-f"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="fab fa-twitter"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="fab fa-linkedin-in"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="fab fa-pinterest"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="fab fa-telegram-plane"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="far fa-envelope"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="more-button float-end">
                                            <a href="blog-single.html"><span class="icon-options"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-6">
                            <!-- post -->
                            <div class="post post-list clearfix">
                                <div class="thumb rounded">
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/latest-sm-2.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details">
                                    <ul class="meta list-inline mb-3">
                                        <li class="list-inline-item"><a href="#"><img
                                                        src="<?php echo get_template_directory_uri() ?>/assets/images/other/author-sm.png"
                                                        class="author"
                                                        alt="author"/>کاتن</a></li>
                                        <li class="list-inline-item"><a href="#">سبک زندگی</a></li>
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                    <h5 class="post-title"><a href="blog-single.html">با این 7 نکته بر هنر طبیعت
                                            مسلط شوید</a></h5>
                                    <p class="excerpt mb-0">آرامشی شگفت انگیز تمام روحم را گرفته است، مثل این صبح
                                        های شیرین</p>
                                    <div class="post-bottom clearfix d-flex align-items-center">
                                        <div class="social-share me-auto">
                                            <button class="toggle-button icon-share"></button>
                                            <ul class="icons list-unstyled list-inline mb-0">
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="fab fa-facebook-f"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="fab fa-twitter"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="fab fa-linkedin-in"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="fab fa-pinterest"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="fab fa-telegram-plane"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="far fa-envelope"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="more-button float-end">
                                            <a href="blog-single.html"><span class="icon-options"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-6">
                            <!-- post -->
                            <div class="post post-list clearfix">
                                <div class="thumb rounded">
										<span class="post-format-sm">
											<i class="icon-camrecorder"></i>
										</span>
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/latest-sm-3.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details">
                                    <ul class="meta list-inline mb-3">
                                        <li class="list-inline-item"><a href="#"><img
                                                        src="<?php echo get_template_directory_uri() ?>/assets/images/other/author-sm.png"
                                                        class="author"
                                                        alt="author"/>کاتن</a></li>
                                        <li class="list-inline-item"><a href="#">مد روز</a></li>
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                    <h5 class="post-title"><a href="blog-single.html">حقایقی در مورد تجارت که به
                                            موفقیت شما کمک می کند</a></h5>
                                    <p class="excerpt mb-0">آرامشی شگفت انگیز تمام روحم را گرفته است، مثل این صبح
                                        های شیرین</p>
                                    <div class="post-bottom clearfix d-flex align-items-center">
                                        <div class="social-share me-auto">
                                            <button class="toggle-button icon-share"></button>
                                            <ul class="icons list-unstyled list-inline mb-0">
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="fab fa-facebook-f"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="fab fa-twitter"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="fab fa-linkedin-in"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="fab fa-pinterest"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="fab fa-telegram-plane"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="far fa-envelope"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="more-button float-end">
                                            <a href="blog-single.html"><span class="icon-options"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-6">
                            <!-- post -->
                            <div class="post post-list clearfix">
                                <div class="thumb rounded">
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/latest-sm-4.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details">
                                    <ul class="meta list-inline mb-3">
                                        <li class="list-inline-item"><a href="#"><img
                                                        src="<?php echo get_template_directory_uri() ?>/assets/images/other/author-sm.png"
                                                        class="author"
                                                        alt="author"/>کاتن</a></li>
                                        <li class="list-inline-item"><a href="#">سیاست</a></li>
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                    <h5 class="post-title"><a href="blog-single.html">نور شما نزدیک به توقف شدن
                                            است:</a></h5>
                                    <p class="excerpt mb-0">آرامشی شگفت انگیز تمام روحم را گرفته است، مثل این صبح
                                        های شیرین</p>
                                    <div class="post-bottom clearfix d-flex align-items-center">
                                        <div class="social-share me-auto">
                                            <button class="toggle-button icon-share"></button>
                                            <ul class="icons list-unstyled list-inline mb-0">
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="fab fa-facebook-f"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="fab fa-twitter"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="fab fa-linkedin-in"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="fab fa-pinterest"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="fab fa-telegram-plane"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="far fa-envelope"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="more-button float-end">
                                            <a href="blog-single.html"><span class="icon-options"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div
                    >
                    <!-- بارگذاری بیشتر button -->
                    <div class="text-center">
                        <button class="btn btn-simple">بارگذاری بیشتر</button>
                    </div>

                </div>

            </div>
            <div class="col-lg-4">

                <!-- sidebar -->
                <div class="sidebar">
                    <!-- widget about -->
                    <div class="widget rounded">
                        <div class="widget-about data-bg-image text-center"
                             data-bg-image="<?php echo get_template_directory_uri() ?>/assets/images/map-bg.png">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/logo.svg" alt="logo"
                                 class="mb-4"/>
                            <p class="mb-4">سلام، ما نویسنده محتوا هستیم که مجذوب مطالب مد روز، سلبریتی و سبک زندگی
                                هستیم. ما به مشتریان کمک می کنیم محتوای مناسب را برای افراد مناسب ارائه دهند.</p>
                            <ul class="social-icons list-unstyled list-inline mb-0">
                                <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fab fa-instagram"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fab fa-pinterest"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fab fa-medium"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fab fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <!-- widget مطالب محبوب -->
                    <div class="widget rounded">
                        <div class="widget-header text-center">
                            <h3 class="widget-title">مطالب محبوب</h3>
                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/wave.svg"
                                 class="wave" alt="wave"/>
                        </div>
                        <div class="widget-content">
                            <!-- post -->
                            <div class="post post-list-sm circle">
                                <div class="thumb circle">
                                    <span class="number">1</span>
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/tabs-1.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="blog-single.html">3 راه آسان برای سریعتر
                                            کردن آیفون</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- post -->
                            <div class="post post-list-sm circle">
                                <div class="thumb circle">
                                    <span class="number">2</span>
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/tabs-2.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="blog-single.html">یک روش فوق العاده آسان که
                                            برای همه کار می کند</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- post -->
                            <div class="post post-list-sm circle">
                                <div class="thumb circle">
                                    <span class="number">3</span>
                                    <a href="blog-single.html">
                                        <div class="inner">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/posts/tabs-3.jpg"
                                                 alt="post-title"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="details clearfix">
                                    <h6 class="post-title my-0"><a href="blog-single.html">10 راه برای شروع فوری
                                            فروش مبلمان</a></h6>
                                    <ul class="meta list-inline mt-1 mb-0">
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- widget categories -->
                    <div class="widget rounded">
                        <div class="widget-header text-center">
                            <h3 class="widget-title">کاوش موضوعات</h3>
                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/wave.svg"
                                 class="wave" alt="wave"/>
                        </div>
                        <div class="widget-content">
                            <ul class="list">
                                <li><a href="#">سبک زندگی</a><span>(5)</span></li>
                                <li><a href="#">الهام بخش</a><span>(2)</span></li>
                                <li><a href="#">مد روز</a><span>(4)</span></li>
                                <li><a href="#">سیاست</a><span>(1)</span></li>
                                <li><a href="#">پرطرفدار</a><span>(7)</span></li>
                                <li><a href="#">فرهنگ</a><span>(3)</span></li>
                            </ul>
                        </div>

                    </div>

                    <!-- widget newsletter -->
                    <div class="widget rounded">
                        <div class="widget-header text-center">
                            <h3 class="widget-title">خبرنامه</h3>
                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/wave.svg"
                                 class="wave" alt="wave"/>
                        </div>
                        <div class="widget-content">
                            <span class="newsletter-headline text-center mb-3">به 70,000 مشترک ما بپیوندید!</span>
                            <form>
                                <div class="mb-2">
                                    <input class="form-control w-100 text-center"
                                           placeholder="ایمیل خود را بنویسید ..." type="email">
                                </div>
                                <button class="btn btn-default btn-full" type="submit">ثبت نام</button>
                            </form>
                            <span class="newsletter-privacy text-center mt-3">با ثبت نام، با <a href="#">سیاست حفظ حریم خصوصی ما</a> موافقت می کنید</span>
                        </div>
                    </div>

                    <!-- widget post carousel -->
                    <div class="widget rounded">
                        <div class="widget-header text-center">
                            <h3 class="widget-title">جشن</h3>
                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/wave.svg"
                                 class="wave" alt="wave"/>
                        </div>
                        <div class="widget-content">
                            <div class="post-carousel-widget">
                                <!-- post -->
                                <div class="post post-carousel">
                                    <div class="thumb rounded">
                                        <a href="category.html" class="category-badge position-absolute">چگونه</a>
                                        <a href="blog-single.html">
                                            <div class="inner">
                                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/widgets/widget-carousel-1.jpg"
                                                     alt="post-title"/>
                                            </div>
                                        </a>
                                    </div>
                                    <h5 class="post-title mb-0 mt-4"><a href="blog-single.html">5 راه آسان که می
                                            توانید آینده را به موفقیت تبدیل کنید</a></h5>
                                    <ul class="meta list-inline mt-2 mb-0">
                                        <li class="list-inline-item"><a href="#">کاتن</a></li>
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                                <!-- post -->
                                <div class="post post-carousel">
                                    <div class="thumb rounded">
                                        <a href="category.html"
                                           class="category-badge position-absolute">پرطرفدار</a>
                                        <a href="blog-single.html">
                                            <div class="inner">
                                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/widgets/widget-carousel-2.jpg"
                                                     alt="post-title"/>
                                            </div>
                                        </a>
                                    </div>
                                    <h5 class="post-title mb-0 mt-4"><a href="blog-single.html">با این 7 نکته بر هنر
                                            طبیعت مسلط شوید</a></h5>
                                    <ul class="meta list-inline mt-2 mb-0">
                                        <li class="list-inline-item"><a href="#">کاتن</a></li>
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                                <!-- post -->
                                <div class="post post-carousel">
                                    <div class="thumb rounded">
                                        <a href="category.html" class="category-badge position-absolute">چگونه</a>
                                        <a href="blog-single.html">
                                            <div class="inner">
                                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/widgets/widget-carousel-1.jpg"
                                                     alt="post-title"/>
                                            </div>
                                        </a>
                                    </div>
                                    <h5 class="post-title mb-0 mt-4"><a href="blog-single.html">5 راه آسان که می
                                            توانید آینده را به موفقیت تبدیل کنید</a></h5>
                                    <ul class="meta list-inline mt-2 mb-0">
                                        <li class="list-inline-item"><a href="#">کاتن</a></li>
                                        <li class="list-inline-item">30 اردیبهشت 1401</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- carousel arrows -->
                            <div class="slick-arrows-bot">
                                <button type="button" data-role="none"
                                        class="carousel-botNav-prev slick-custom-buttons" aria-label="بعدی"><i
                                            class="icon-arrow-right"></i></button>
                                <button type="button" data-role="none"
                                        class="carousel-botNav-next slick-custom-buttons" aria-label="قبلی"><i
                                            class="icon-arrow-left"></i></button>
                            </div>
                        </div>
                    </div>

                    <!-- widget advertisement -->
                    <div class="widget no-container rounded text-md-center">
                        <span class="ads-title">- تبلیغات -</span>
                        <a href="#" class="widget-ads">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/ads/ad-360.png"
                                 alt="Advertisement"/>
                        </a>
                    </div>

                    <!-- widget tags -->
                    <div class="widget rounded">
                        <div class="widget-header text-center">
                            <h3 class="widget-title">برچسب ها</h3>
                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/wave.svg"
                                 class="wave" alt="wave"/>
                        </div>
                        <div class="widget-content">
                            <a href="#" class="tag">#پرطرفدار</a>
                            <a href="#" class="tag">#فیلم</a>
                            <a href="#" class="tag">#ویژه</a>
                            <a href="#" class="tag">#گالری</a>
                            <a href="#" class="tag">#سلبریتی</a>
                        </div>
                    </div>

                </div>

            </div>


        </div>

    </div>
</section>

<!-- instagram feed -->
<div class="instagram">
    <div class="container-xl">
        <!-- button -->
        <a href="#" class="btn btn-default btn-instagram">@Katen در اینستاگرام</a>
        <!-- images -->
        <div class="instagram-feed d-flex flex-wrap">
            <div class="insta-item col-sm-2 col-6 col-md-2">
                <a href="#">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/insta/insta-1.jpg"
                         alt="insta-title"/>
                </a>
            </div>
            <div class="insta-item col-sm-2 col-6 col-md-2">
                <a href="#">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/insta/insta-2.jpg"
                         alt="insta-title"/>
                </a>
            </div>
            <div class="insta-item col-sm-2 col-6 col-md-2">
                <a href="#">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/insta/insta-3.jpg"
                         alt="insta-title"/>
                </a>
            </div>
            <div class="insta-item col-sm-2 col-6 col-md-2">
                <a href="#">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/insta/insta-4.jpg"
                         alt="insta-title"/>
                </a>
            </div>
            <div class="insta-item col-sm-2 col-6 col-md-2">
                <a href="#">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/insta/insta-5.jpg"
                         alt="insta-title"/>
                </a>
            </div>
            <div class="insta-item col-sm-2 col-6 col-md-2">
                <a href="#">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/insta/insta-6.jpg"
                         alt="insta-title"/>
                </a>
            </div>
        </div>
    </div>
</div>

<!-- footer -->
<?php get_footer() ?>

</div><!-- end site wrapper -->

<!-- search popup area -->
<div class="search-popup">
    <!-- close button -->
    <button type="button" class="btn-close" aria-label="Close"></button>
    <!-- content -->
    <div class="search-content">
        <div class="text-center">
            <h3 class="mb-4 mt-0">ESC را فشار دهید تا بسته شود</h3>
        </div>
        <!-- form -->
        <form class="d-flex search-form">
            <input class="form-control me-2" type="search" placeholder="جستجو کنید و اینتر را فشار دهید ..."
                   aria-label="Search">
            <button class="btn btn-default btn-lg" type="submit"><i class="icon-magnifier"></i></button>
        </form>
    </div>
</div>
</div>

<!-- canvas menu -->
<div class="canvas-menu d-flex align-items-end flex-column">
    <!-- close button -->
    <button type="button" class="btn-close" aria-label="Close"></button>

    <!-- logo -->
    <div class="logo">
        <img src="<?php echo get_template_directory_uri() ?>/assets/images/logo.svg" alt="Katen"/>
    </div>

    <!-- menu -->
    <nav>
        <ul class="vertical-menu">
            <li class="active">
                <a href="index.html">صفحه اصلی</a>
                <ul class="submenu">
                    <li><a href="index.html">صفحه اصلی 1</a></li>
                    <li><a href="index-2.html">صفحه اصلی 2</a></li>
                    <li><a href="index-3.html">صفحه اصلی 3</a></li>
                    <li><a href="index-4.html">صفحه اصلی 4</a></li>
                    <li><a href="index-5.html">صفحه اصلی 5</a></li>
                </ul>
            </li>

            <li>
                <a href="#">صفحات</a>
                <ul class="submenu">
                    <li><a href="category.html">دسته بندی</a></li>
                    <li><a href="blog-single.html">جزییات وبلاگ 1</a></li>
                    <li><a href="blog-single-2.html">جزییات وبلاگ 2</a></li>

                </ul>
            </li>
            <li><a href="contact.html">تماس با ما</a></li>
            <li><a href="about.html">درباره ما</a></li>
        </ul>
    </nav>
    <!-- social icons -->
    <ul class="social-icons list-unstyled list-inline mb-0 mt-auto w-100">
        <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-twitter"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-instagram"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-pinterest"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-medium"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-youtube"></i></a></li>
    </ul>
</div>
<?php wp_footer(); ?>
</body>

</html>